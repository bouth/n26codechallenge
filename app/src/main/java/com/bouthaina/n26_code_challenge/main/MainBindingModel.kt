package com.bouthaina.n26_code_challenge.main

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.bouthaina.n26_code_challenge.BR

/**
 * Bindingmodel for [MainActivity].
 *
 *@author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class MainBindingModel : BaseObservable() {

    @get:Bindable
    var graphAdapter: GraphAdapter? = null
        set(graphAdapter) {
            field = graphAdapter
            notifyPropertyChanged(BR.graphAdapter)
        }

    @get:Bindable
    var minRate: String? = null
        set(minRate) {
            field = minRate
            notifyPropertyChanged(BR.minRate)
        }

    @get:Bindable
    var maxRate: String? = null
        set(maxRate) {
            field = maxRate
            notifyPropertyChanged(BR.maxRate)
        }

    @get:Bindable
    var endDate: String? = null
        set(endDate) {
            field = endDate
            notifyPropertyChanged(BR.endDate)
        }

    @get:Bindable
    var startDate: String? = null
        set(startDate) {
            field = startDate
            notifyPropertyChanged(BR.startDate)
        }

    @get:Bindable
    var description: String? = null
        set(description) {
            field = description
            notifyPropertyChanged(BR.description)
        }

    @get:Bindable
    var name: String? = null
        set(name) {
            field = name
            notifyPropertyChanged(BR.name)
        }

    @get:Bindable
    var selectedTab: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.selectedTab)
        }



    @get:Bindable
    var progressBarVisible: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.progressBarVisible)
        }

}