package com.bouthaina.n26_code_challenge.main

import com.bouthaina.n26_code_challenge.data.model.MarketPrice
import com.robinhood.spark.SparkAdapter

/**
 * Adapter for a graphs.
 * Displays prices  in periode of date
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
class GraphAdapter : SparkAdapter() {

    var data: List<MarketPrice>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getY(index: Int): Float {
        return data?.get(index)?.value ?: 0f
    }

    override fun getItem(index: Int): Any? {
        return data?.get(index)
    }

    override fun getCount() = data?.size ?: 0

}