package com.bouthaina.n26_code_challenge.main

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.text.format.DateUtils
import android.util.Log
import android.view.View
import androidx.constraintlayout.solver.SolverVariable
import com.bouthaina.n26_code_challenge.base.BaseViewModel
import com.bouthaina.n26_code_challenge.data.model.ChartsResponse
import com.bouthaina.n26_code_challenge.data.model.MarketPrice
import com.bouthaina.n26_code_challenge.data.model.Period
import com.bouthaina.n26_code_challenge.data.rest.RestClient
import com.bouthaina.n26_code_challenge.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.Duration
import java.util.*
import kotlin.collections.ArrayList

/**
 * ViewModel from [MainActivity]
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
class MainActivityViewModel : BaseViewModel<ActivityMainBinding>() {

    lateinit var chartResponse: ChartsResponse<*>
    var prices = ArrayList<MarketPrice>()
    var adapter = GraphAdapter()

    var bindingModel = MainBindingModel()

    override fun setupDataBinding(binding: ActivityMainBinding) {
        binding.viewModel = this
       loadPrices(Period.week)
    }

    fun loadPrices(period: Period) {
        when (period) {
            Period.week -> loadData(period, "day", (System.currentTimeMillis() - DateUtils.WEEK_IN_MILLIS) / 1000)
            Period.month -> loadData(period, "day", (System.currentTimeMillis() - DateUtils.WEEK_IN_MILLIS * 4) / 1000)
            Period.year -> loadData(period, "month", (System.currentTimeMillis() - DateUtils.YEAR_IN_MILLIS) / 1000)
        }
    }

    fun setSelectedTab(tab: Int) {
        bindingModel.selectedTab = tab
        prices.clear()
            when (tab) {
                0 -> { loadPrices(Period.week) }
                1 ->  { loadPrices(Period.month) }
                2 ->  { loadPrices(Period.year) }

        }
    }

    fun updateGraph(period: Period) {
        bindingModel.name = chartResponse.name
        bindingModel.description = chartResponse.description
        adapter.data = prices
        showData(prices, period)
    }

    fun showData(data: List<MarketPrice>, period: Period) {
        bindingModel.graphAdapter = adapter
        val firstItem = data[0]
        val lastItem = data[data.lastIndex]

        bindingModel.minRate = firstItem.value.toString() + " " + Currency.getInstance(chartResponse.unit).symbol.toString()
        bindingModel.maxRate = lastItem.value.toString() + " " +  Currency.getInstance(chartResponse.unit).symbol.toString()

        val flags: Int = when (period) {
            Period.week -> DateUtils.FORMAT_SHOW_TIME and DateUtils.FORMAT_SHOW_DATE
            Period.month -> DateUtils.FORMAT_SHOW_WEEKDAY and DateUtils.FORMAT_SHOW_DATE
            Period.year -> DateUtils.FORMAT_SHOW_DATE and DateUtils.FORMAT_SHOW_YEAR
            else -> throw IllegalArgumentException()
        }

        bindingModel.startDate = DateUtils.formatDateTime(activity as MainActivity, firstItem.timeSpan * 1000, flags)
        bindingModel.endDate = DateUtils.formatDateTime(activity as MainActivity, lastItem.timeSpan * 1000, flags)
    }


    fun loadData(period: Period, periodKey: String, startTimestamp: Long) {
        bindingModel.progressBarVisible = true

        run {
            val retrofit = Retrofit.Builder().baseUrl("https://api.blockchain.info/").addConverterFactory(GsonConverterFactory.create()).build()
            val requestInterface = retrofit.create(RestClient::class.java)

            val call = requestInterface.getMarketPrices(periodKey, startTimestamp).also {
                it.run {
                    enqueue(object : retrofit2.Callback<ChartsResponse<MarketPrice>> {


                        override fun onFailure(call: retrofit2.Call<ChartsResponse<MarketPrice>>, t: Throwable) {
                            bindingModel.progressBarVisible = false
                            showSnackBar(activity as MainActivity, "An error occurred Please try again later", "OK", View.OnClickListener {})
                        }

                        @SuppressLint("ShowToast")
                        override fun onResponse(call: retrofit2.Call<ChartsResponse<MarketPrice>>, response: Response<ChartsResponse<MarketPrice>>) {
                            prices.clear()
                            chartResponse = response.body()!!
                            prices.addAll(response.body()!!.values)
                            updateGraph(period)
                            bindingModel.progressBarVisible = false
                        }
                    })
                }
            }
        }
    }

    fun showSnackBar(activity: MainActivity, message: String, action: String? = null,
                     actionListener: View.OnClickListener? = null, duration: Int = Snackbar.LENGTH_SHORT) {
        val snackBar = Snackbar.make(activity.findViewById(android.R.id.content), message, duration)
                .setBackgroundTint(Color.parseColor("#CC000000"))
                .setTextColor(Color.WHITE)
        if (action != null && actionListener!=null) {
            snackBar.setAction(action, actionListener)
        }
        snackBar.show()
    }
}