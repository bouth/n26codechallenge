package com.bouthaina.n26_code_challenge.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.bouthaina.n26_code_challenge.R

class SplashActivity : Activity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        val secondsDelayed = 1
        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }, (secondsDelayed * 1000).toLong())
    }
}