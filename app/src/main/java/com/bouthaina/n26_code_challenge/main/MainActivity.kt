package com.bouthaina.n26_code_challenge.main

import com.bouthaina.n26_code_challenge.R
import com.bouthaina.n26_code_challenge.base.BaseActivity

/**
 * we show the graph in the mainActivity
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
open class MainActivity : BaseActivity<MainActivityViewModel>(R.layout.activity_main,
        R.id.activity_main_layout_binding, MainActivityViewModel()) {
}