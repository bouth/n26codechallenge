package com.bouthaina.n26_code_challenge.data.model

/**
 * Enum for period of week , month and year
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
enum class Period {
    week, month, year
}