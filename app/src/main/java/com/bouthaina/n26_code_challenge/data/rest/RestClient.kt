package com.bouthaina.n26_code_challenge.data.rest

import com.bouthaina.n26_code_challenge.data.model.ChartsResponse
import com.bouthaina.n26_code_challenge.data.model.MarketPrice
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Single

/**
 * interface RestCLient for API request
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
interface RestClient {

    @GET("/charts/market-price?format=json")
    fun getMarketPrices(
            @Query("period") period: String,
            @Query("start") startTimestamp : Long
    ): Call<ChartsResponse<MarketPrice>>

}