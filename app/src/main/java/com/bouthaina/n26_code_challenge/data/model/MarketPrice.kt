package com.bouthaina.n26_code_challenge.data.model

import com.google.gson.annotations.SerializedName

/**
 * Market price model
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
data class MarketPrice (

        @SerializedName("x")
        val timeSpan: Long,

        @SerializedName("y")
        val value: Float

)