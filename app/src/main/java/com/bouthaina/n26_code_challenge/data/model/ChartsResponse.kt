package com.bouthaina.n26_code_challenge.data.model


/**
 * ChartsResponse model
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
data class ChartsResponse<T>(

        val status: String,
        val name: String,
        val unit: String,
        val period: String,
        val description: String,
        val values: List<T>

)