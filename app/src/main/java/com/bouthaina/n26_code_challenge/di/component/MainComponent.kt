package com.bouthaina.n26_code_challenge.di.component

import com.bouthaina.n26_code_challenge.main.MainActivity
import com.bouthaina.n26_code_challenge.di.module.MainModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(MainModule::class))
interface MainComponent {
    fun inject(view: MainActivity)
}