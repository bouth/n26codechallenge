package com.bouthaina.n26_code_challenge.databindingadapters;

import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

import com.bouthaina.n26_code_challenge.base.BaseApplication;
import com.bouthaina.n26_code_challenge.main.GraphAdapter;
import com.google.android.material.tabs.TabLayout;
import com.robinhood.spark.SparkView;

/**
 * Custom BindingAdapters for native Android Views
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
public class AndroidDataBindingAdapter {

    @BindingAdapter("adapter")
    public static <VM> void setSparkViewAdapter(SparkView sparkView, GraphAdapter adapter) {
        if (adapter != null && sparkView.getAdapter() == null) {
            sparkView.setAdapter(adapter);
        }
    }

    @BindingAdapter(value = {"selectedTab", "selectedTabAttrChanged", "onTabChanged"}, requireAll = false)
    public static void bindSelectedTab(final TabLayout tabLayout, final int selectedTab,
                                       final InverseBindingListener selectedTabAttrChanged,
                                       final OnTabChangedListener listener) {
        TabLayout.OnTabSelectedListener tabSelectedListener =
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        if(listener != null){
                            listener.onTabChanged(tab.getPosition());
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                    }
                };
        tabLayout.addOnTabSelectedListener(tabSelectedListener);
    }

    public interface OnTabChangedListener {
        void onTabChanged(int value);
    }

    @InverseBindingAdapter(attribute = "selectedTab", event = "selectedTabAttrChanged")
    public static int captureSelectedTab(TabLayout tabLayout) {
        return tabLayout.getSelectedTabPosition();
    }

    @BindingAdapter(value = {"tabTextColor", "tabSelectedTextColor", "tabIndicatorColor"}, requireAll = false)
    public static void setTabColors(TabLayout tabLayout, int defaultColor, int selectedColor, int indicatorColor) {
        int tabDefaultColor = ContextCompat.getColor(BaseApplication.Companion.getInstance(), defaultColor);
        int tabSelectedColor = ContextCompat.getColor(BaseApplication.Companion.getInstance(), selectedColor);
        int tabIndicatorColor = ContextCompat.getColor(BaseApplication.Companion.getInstance(), indicatorColor);
        tabLayout.setTabTextColors(tabDefaultColor, tabSelectedColor);
        tabLayout.setSelectedTabIndicatorColor(tabIndicatorColor);
    }

    @BindingAdapter("visibleElseGone")
    public static void visible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
