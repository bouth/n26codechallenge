package com.bouthaina.n26_code_challenge.base

import android.app.Activity
import android.content.*
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import butterknife.ButterKnife
import com.bouthaina.n26_code_challenge.R


/**
 * BaseActivity for all Activities.
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
abstract class BaseActivity<VM : BaseViewModel<*>>(val activityLayoutId: Int?, val id: Int?, private var viewModel: VM) : AppCompatActivity() {

    private lateinit var networkBroadcastReceiver: BroadcastReceiver

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.activity = this
        this.setContentView(activityLayoutId!!)

        ButterKnife.bind(this)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        val content: View = this.findViewById(id!!)
        viewModel.setDataBinding(DataBindingUtil.bind(content))

        viewModel.setDataBinding(DataBindingUtil.bind(content))

        //setup Actionbar
        if (supportActionBar != null) {
            supportActionBar!!.hide()
        }
    }
}