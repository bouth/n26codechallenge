package com.bouthaina.n26_code_challenge.base

import android.app.Application
import android.content.Context
import androidx.annotation.NonNull
import com.bouthaina.n26_code_challenge.di.component.DaggerMainComponent
import com.bouthaina.n26_code_challenge.di.component.MainComponent

/**
 * Application which handles Services and other Setups before starting the first Activity
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
class BaseApplication : Application() {

    lateinit var graph: MainComponent

    override fun onCreate() {
        super.onCreate()
        instance = this
        graph = DaggerMainComponent.builder().build()
    }

    companion object {
        lateinit var instance: BaseApplication
            private set
    }
}