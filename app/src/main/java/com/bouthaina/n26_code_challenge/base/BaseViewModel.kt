package com.bouthaina.n26_code_challenge.base

import androidx.annotation.Nullable
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import rx.*
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action0
import rx.functions.Action1
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

/**
 * BaseViewModel for  BaseActivity.
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
abstract class BaseViewModel<VDB : ViewDataBinding> : ViewModel() {

    protected var viewDataBinding: VDB? = null
    var activity: BaseActivity<*>? = null

    protected abstract fun setupDataBinding(binding: VDB)

    @SuppressWarnings("unchecked")
    fun setDataBinding(@Nullable binding: ViewDataBinding?) {
        if (binding != null) {
            this.viewDataBinding = binding as VDB?
            setupDataBinding(binding as VDB)
        }
    }
}